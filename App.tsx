import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import { BrowserRouter, Router, Route, Switch, withRouter } from 'react-router-dom';
import { createBrowserHistory } from 'history';

import CardImage from './components/CardImage'

const history = createBrowserHistory();

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (

      <SafeAreaProvider>
          <Navigation colorScheme={colorScheme} />
          <StatusBar />
            <Router history={history}>
              <Switch>
              <Route exact path="/CardImage">
              <CardImage />
              </Route>
              </Switch>
            </Router>
          </SafeAreaProvider>
    );
  }
}