import * as React from 'react';
import { StyleSheet, TextInput, ScrollView, FlatList, VirtualizedList, Dimensions, Image } from 'react-native';
import { Entypo, SimpleLineIcons, Ionicons, MaterialCommunityIcons, FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons'; 
import { Avatar, Card, Title, Paragraph, Button } from 'react-native-paper';
import { Text, View } from '../components/Themed';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import globo from '../assets/images/globo.jpg'
import pai1 from '../assets/images/pai1.jpg'
import pai2 from '../assets/images/pai2.jpg'
import pai3 from '../assets/images/pai3.jpg'

export default function CardImage() {
    return (
        <ScrollView style={styles.scrollView}>
            <View style={styles.container}>
            <View style={styles.textWrapper}>
            <View style={styles.prueba}>
            <Ionicons name="ios-arrow-back" size={24} color="blue" />
                <Text style={styles.title}>Back</Text>
                <Text style={styles.title2}>Hot air ballon</Text>
                <Entypo style={styles.puntos} name="dots-three-horizontal" size={24} color="blue" />
            </View>
            </View>
            </View>

            <Image source={globo} style={styles.imagen} />

            <View style={styles.likes}>
            <SimpleLineIcons name="eye" size={18} color="gray" />
            <Text style={{ fontSize: 15, color: "gray", marginLeft: 2 }}>8975</Text>

            <Text style={{ fontSize: 15, color: "gray", marginLeft: 160 }}>75</Text>
            <MaterialCommunityIcons name="message" size={18} color="gray" />

            <Text style={{ fontSize: 15, color: "gray", marginLeft: 15 }}>705</Text>
            <FontAwesome name="heart" size={18} color="gray" />
            </View>

            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
            
            <View>
            <Text style={styles.comments}>Comments</Text>
            </View>

            <View style={styles.container3}>
            <Avatar.Image size={50} source={pai1} />
            <View style={styles.contariner5}>
                <Text style={styles.nombre}>Cameron Sanchez</Text> 
                <Text style={styles.texto}>2 horas ago</Text> 
            </View>
            <View>
            
            <View style={styles.container4}>  
            <Text style={{ fontSize: 18 }}>lorem Lorem Lorem Lorem     
            </Text>
            <Text style={{ color: "blue", marginTop: 10, fontWeight: 'bold'}}>Replay</Text>
            </View>
            </View>
            
            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
            
            <View style={styles.container3}>
            <Avatar.Image size={50} source={pai2} />
            <View style={styles.contariner5}>
                <Text style={styles.nombre}>Cameron Sanchez</Text> 
                <Text style={styles.texto}>2 horas ago</Text> 
            </View>
            </View>

            <View style={styles.container4}>  
            <Text style={{ fontSize: 18 }}>lorem Lorem Lorem Lorem     
            </Text>
            <Text style={{ color: "blue", marginTop: 10, fontWeight: 'bold'}}>Replay</Text>
            </View>
            
            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
            
            <View style={styles.container3}>
            <Avatar.Image size={50} source={pai3} />
            <View style={styles.contariner5}>
                <Text style={styles.nombre}>Cameron Sanchez</Text> 
                <Text style={styles.texto}>2 horas ago</Text> 
            </View>
            </View>

            <View style={styles.container4}>  
            <Text style={{ fontSize: 18 }}>lorem Lorem Lorem Lorem     
            </Text>
            <Text style={{ color: "blue", marginTop: 10, fontWeight: 'bold'}}>Replay</Text>
            </View>

            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
            
            <View style={styles.contariner6}>
            <TextInput style={styles.input} placeholder={'Escribe tu comentario'} />
            <Feather name="send" size={22} color="blue" />
            </View>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
    },
    textWrapper: {
        height: hp('8%'),
        width: wp('100%')
    },
    prueba: {
        flex: 1,
        display:'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    title: {
        fontSize: 18,
        color: 'blue',
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        marginTop: 5,
        marginLeft: -45
    },
    title2: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 40,
    },
    comments: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    puntos: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        marginTop: 5,
    },
    separator: {
        marginTop: 10,
        height: 2,
        width: '100%',
    },
    scrollView: {
        marginHorizontal: 10,
    },
    likes: {
        marginTop: 10,
        display: 'flex',
        flexDirection: 'row',
    },    
    imagen: {
        display: 'flex',
        flexDirection: 'column',
        width: 310,
        height: 300,
        borderRadius: 10,      
    },
    container3: {
        display: 'flex',
        flexDirection: 'column',
        marginTop: 20,
    },
    nombre: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    texto: {
        margin: 2,
    },
    contariner5: {
        display: 'flex',
        flexDirection: 'column',
        marginLeft: 60,
        marginTop: -50,
    },
    container4: {
        display:'flex',
        flexDirection: 'column',
        marginTop: 30,
        marginLeft: 60,
    },
    contariner6: {
        flex: 1,
        display:'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        padding: 20, 
        backgroundColor: '#eee',
    },
    input: {
        width: '90%',
        height: 30,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 10,
        display:'flex',
        fontSize: 12,
        fontFamily: 'arial',
        color: 'black',
        backgroundColor: 'white',      
    }
});
