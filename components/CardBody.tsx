import * as React from 'react';
import { StyleSheet, TextInput, ScrollView, FlatList, VirtualizedList, Dimensions, Image, TouchableOpacity } from 'react-native';
import { AntDesign, EvilIcons } from '@expo/vector-icons'; 
import { Avatar, Card, Title, Paragraph, Button } from 'react-native-paper';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Text, View } from '../components/Themed';
import cara from '../assets/images/cara.jpg'
import pai1 from '../assets/images/pai1.jpg'
import pai2 from '../assets/images/pai2.jpg'
import pai3 from '../assets/images/pai3.jpg'

export default function CardBody() {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Other post</Text>
            
            <Card>
            <View style={styles.container3}>
            <Avatar.Image size={40} source={cara} />
            <TouchableOpacity onPress={() => alert('Hello, world!')} style={styles.button}>
                <Text style={styles.buttonText}>Follow</Text>
            </TouchableOpacity>
            </View>

            <View style={styles.contenombre}>
            <Text style={styles.cameron}>Cameron Sanchez</Text> 
            <Text style={styles.texto}>2 horas ago</Text> 
            </View>
            
            <View style={styles.container4}>
            <Text style={{ color: "blue", margin: 10 }}>#Picture, #Relax</Text>
            <Text style={{ fontSize: 15, margin: 10 }}>lorem
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui veniam ipsum amet accusamus soluta, quae nemo eos consequatur quo reprehenderit perspiciatis. Asperiores dolorum accusamus culpa, quis odio qui amet laboriosam.   
            </Text>
            </View>
            
            <View style={styles.textWrapper}>
                <View style={styles.container2}>
                <Image source={pai1} style={styles.image} />
                <Image source={pai2} style={styles.image} />
                <Image source={pai3} style={styles.image} />
                </View>
            </View>
            </Card>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 2
    },
    textWrapper: {
        height: hp('38%'),
        width: wp('98%')
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    contenombre: {
        display:'flex',
        flexDirection: 'column',
    },
    texto: {
        fontSize: 10,
        marginLeft: 66,
        marginTop: 6
    },
    cameron: {
        fontSize: 18,
        marginLeft: 61,
        marginTop: -50
    },
    image: {
        display: 'flex',
        flex: 1,
        width: '100%',
        height: '100%',
        margin: 5,
        justifyContent: 'flex-start',
        marginBottom: 5,
        flexDirection: 'row',
        borderRadius: 10,
    },
    container2: {
        flex: 1,
        backgroundColor: '#fff',
        margin: 5,
        flexDirection:'row',
        marginTop: 20,
        marginBottom: 20
    },
    container3: {
        flex: 1,
        display:'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        margin: 10, 
        marginBottom: 5,
    },
    button: {
        display: 'flex' ,
        flexDirection: 'row',
        textAlign: 'left',
        justifyContent: 'flex-end',
        backgroundColor: "white",
        color: 'blue',
        borderColor: 'red',
        padding: 10,
        borderRadius: 5,        
    },
    buttonText: {
        display: 'flex', 
        fontSize: 10,
        height: 33,
        color: 'blue',
        textAlign: 'left',
        borderColor: 'blue',
        borderStyle: 'solid',
        borderWidth: 1,
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 5,
        marginLeft: 49,
    }, 
    container4: {
        display: 'flex',
    }
});
