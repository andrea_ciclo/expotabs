import * as React from 'react';
import { StyleSheet, TextInput, ScrollView, Image } from 'react-native';
import { AntDesign, EvilIcons, FontAwesome } from '@expo/vector-icons'; 
import { Avatar, Card, Title, Paragraph, Button } from 'react-native-paper';
import { Text, View } from '../components/Themed';
// import { withRouter } from "react-router-dom";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import cara from '../assets/images/cara.jpg'
import CardBody from './CardBody'
import pai1 from '../assets/images/pai1.jpg'
import pai2 from '../assets/images/pai2.jpg'
import pai3 from '../assets/images/pai3.jpg'

export default function Header() {
    return (
        <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
            <AntDesign name="plus" size={20} color="blue" />
            <View style={styles.textWrapper}>
            <View style={styles.post}>
                <View style={styles.prueba}>
            <Text style={styles.texto}>My Posts</Text>
                <Avatar.Image style={styles.cara} size={40} source={cara} />
            </View>
                </View> 
            <View style={styles.searchcontainer}>
            <TextInput style={{ height: 30, borderColor: '#eee', borderWidth: 1, borderRadius: 5, backgroundColor:'#eee', margin: '10' }}
                />
                <EvilIcons style={styles.search} name="search" size={20} color= 'grey' placeholder="Search" />
            </View>
            <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                <View style={styles.container2}>
                <Image source={pai1} style={styles.image} />
                <Image source={pai2} style={styles.image} />
                <Image source={pai3} style={styles.image} />
                </View>
            </View>
                <View>
                <Text style={{ fontSize: 12, color: "black", marginLeft: 20, }}>705</Text>
                <FontAwesome style={{ fontSize: 12, color: "black", marginLeft: 45, marginTop: -10, }} name="heart" size={18} color="black" />
                </View>

                <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                <CardBody/>
            
        </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: { 
        flex: 1,
        margin: 2
    },
    textWrapper: {
        height: hp('48%'),
        width: wp('98%')
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginTop: 10,
        height: 2,
        width: '100%',
    },
    scrollView: {
        marginHorizontal: 1,
    },
    post: {
        display: 'flex', 
        flexDirection: 'row',
        margin: 10, 
        marginBottom: 5,
    },
    prueba: {
        flex: 1,
        display:'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    },
    texto: {
        display:'flex',
        fontSize: 25,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    cara:{
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        marginTop: 5,
        // marginLeft: 1180,
    },
    container2: {
        flex: 1,
        backgroundColor: '#fff',
        margin: 5,
        flexDirection:'row',
        marginTop: 10, 
    }, 
    image: {
        display: 'flex',
        flex: 1,
        width: '100%',
        height: '100%',
        margin: 5,
        justifyContent: 'flex-start',
        marginBottom: 5,
        flexDirection: 'row',
        borderRadius: 10,
    },
    searchcontainer: {
        
    },
    search: {
        marginRight: -150,
        marginTop: -28,
    }
});
