import * as React from 'react';
import { StyleSheet, TextInput, ScrollView, FlatList, VirtualizedList, Dimensions, Image } from 'react-native';
import { Entypo, MaterialIcons } from '@expo/vector-icons'; 
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Avatar, Card, Title, Paragraph, Button } from 'react-native-paper';
import { Text, View } from '../components/Themed';

import cara1 from '../assets/images/cara.jpg';
import pai1 from '../assets/images/pai1.jpg'
import pai2 from '../assets/images/pai2.jpg'
import pai3 from '../assets/images/pai3.jpg'

export default function Chat() {
    return (
        <ScrollView style={styles.scrollView}>
            <View style={styles.container}>
                <Text>22 December, 2019</Text>
                
                <View style={styles.textWrapper}>
                    <View style={styles.container2}>
                    <Text style={styles.texto}>My Post</Text>
                        <Entypo style={styles.message} name="new-message" size={27} />
                </View>   
                <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                
                    <View style={styles.chatcontainer}>
                        <Image source={pai1} style={styles.imagen} />
                        <Text style={styles.texto1}>Ronald Robert</Text>
                    
                    <View style={styles.navegacion}>
                        <Text style={styles.day}>Yesterday</Text>
                        <MaterialIcons name="navigate-next" size={21} color="gray" />
                    </View>
                        <Text style={styles.texto2}>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </Text>
                    </View>
                    
                    <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                    <View style={styles.chatcontainer}>
                        <Image source={pai2} style={styles.imagen} />
                        <Text style={styles.texto1}>Ronald Robert</Text>

                        <View style={styles.navegacion}>
                        <Text style={styles.day}>10/20/20</Text>
                        <MaterialIcons name="navigate-next" size={21} color="gray" />
                    </View>
                        <Text style={styles.texto2}>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </Text>
                    </View>
                    <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                    <View style={styles.chatcontainer}>
                        <Image source={pai3} style={styles.imagen} />
                        <Text style={styles.texto1}>Ronald Robert</Text>

                        <View style={styles.navegacion}>
                        <Text style={styles.day}>20/20/20</Text>
                        <MaterialIcons name="navigate-next" size={21} color="gray" />
                    </View>
                        <Text style={styles.texto2}>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </Text>
                    </View>
                    <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                    <View style={styles.chatcontainer}>
                        <Image source={pai1} style={styles.imagen} />
                        <Text style={styles.texto1}>Ronald Robert</Text>

                        <View style={styles.navegacion}>
                        <Text style={styles.day}>Thuesday</Text>
                        <MaterialIcons name="navigate-next" size={21} color="gray" />
                    </View>
                        <Text style={styles.texto2}>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        </Text>
                    </View>

                    <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                
                <View style={styles.chatcontainer}>
                    <Image source={pai1} style={styles.imagen} />
                    <Text style={styles.texto1}>Ronald Robert</Text>

                <View style={styles.navegacion}>
                    <Text style={styles.day}>Yesterday</Text>
                    <MaterialIcons name="navigate-next" size={21} color="gray" />
                </View>
                    <Text style={styles.texto2}>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    </Text>
                </View>
                </View>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    textWrapper: {
        height: hp('85%'),
        width: wp('96%')
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    separator: {
        marginTop: 10,
        height: 2,
        width: '100%',
    },
    scrollView: {
        marginHorizontal: 10,
    },
    container2: {
        flex: 1,
        display:'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',   
        margin: 5,
        marginTop: 10, 
    }, 
    texto: {
        display:'flex',
        fontSize: 25,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    texto1: {
        fontSize: 20,
        marginLeft: 68,
        marginTop: -55,
    },
    message: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-start',
        marginTop: 5,
        flexDirection: 'row',
        color: 'blue'
    },
    caracontainer:{
        marginTop: 10,
        marginLeft: 560,
    },
    chatcontainer:{
        display: 'flex', 
        flexDirection: 'column',
        margin: 10, 
        marginBottom: 5,
    },
    imagen: {
        display: 'flex',
        width: 50,
        height: 50,
        margin:2,
        borderRadius: 50,
    },
    texto2: {
        display: 'flex',
        flexDirection: 'row',
        marginTop: 6,
        marginLeft: 69,
    },
    day: {
        display: 'flex',
        flexDirection: 'row',
        color: 'gray',
    },
    navegacion: {
        display: 'flex',
        flexDirection: 'row',
        marginLeft: 242,
        marginTop: -20,
    }
});
