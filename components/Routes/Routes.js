import React from 'react'
import { Router, Scene } from 'react-native-router-flux'
import Header from '../Header'
import CardImage from '../CardImage'

const Routes = () => (
    <Router>
        <Scene key = "root">
            <Scene key = "Header" component = {Header} title = "Header" initial = {true} />
            <Scene key = "CardImage" component = {CardImage} title = "CardImage" />
        </Scene>
    </Router>
)
export default Routes